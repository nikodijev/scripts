#!/usr/bin/env python3
#
# Scraping book titles, authors and prices
# from delfi.rs. 

import csv
import requests
from bs4 import BeautifulSoup
import sys 
import time

# total number of pages:1370

filename = 'books.csv' 

if len(sys.argv) < 2:
    print('usage: ./scrape.py n (number of pages)')
    sys.exit()
else:
    if int(sys.argv[1]) < 1:
        sys.exit()
    else:
        number_of_pages = int(sys.argv[1]) + 1

count = 0
data = []
start_time = time.time()

for i in range(1, number_of_pages):
    page = i
    url = 'http://delfi.rs/knjige/knjige_{}.html'.format(page)
    page = requests.get(url).text
    soup = BeautifulSoup(page, 'lxml')
    books = soup.find_all('div', class_='spisak_art_txt')

    for j in range(16):
        product = books[j]
        for title in product.find_all('div', class_='spisak_art_naslov'):
            for author in product.find_all('div', class_='spisak_art_autor'):
                for old_price in product.find_all('span', class_='art_stara_cena'):
                    for new_price in product.find_all('span', class_='art_nova_cena'):
#                        for price in product.find_all('span', class_='spisak_art_cena'): 'strane knjige'
                        values = []
                        values.extend([title.text, author.text, old_price.text, new_price.text])
                        data.append(values)
                        count += 1
                        sys.stdout.write('Pages:({}\{}) Books:({})'.format(i, number_of_pages-1, count))
                        sys.stdout.write('\r')
                        sys.stdout.flush()

end_time = time.time() - start_time

with open(filename, 'wt') as f:
    csv_writer = csv.writer(f)
    csv_writer.writerows(data)

print('Pages:({}\{}) Books:({})'.format(i, number_of_pages-1, count))
print('Finished in: {:.2f}s.'.format(end_time))
